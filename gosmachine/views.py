import json
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from gosuslugi.utils import group_required
from .models import ServiceSchema, UserOrder


def index(request):
    return HttpResponse('Hello')


@group_required('analitics', raise_exception=True)
def scenario_list(request):
    scenarios = ServiceSchema.objects.all()
    context = {
        'scenarios': scenarios
    }
    return render(request, 'analitics/index.html', context)


@group_required('analitics', raise_exception=True)
def scenario(request, scenario_id):
    scenario = ServiceSchema.objects.get(pk=scenario_id)
    context = {
        'scenario': scenario
    }
    return render(request, 'analitics/scenario.html', context)


@group_required('analitics', raise_exception=True)
def scenario_create(request):
    if request.method == 'GET':
        return render(request, 'analitics/scenario_create.html')
    elif request.method == 'POST':
        scenario = ServiceSchema.objects.create(name=request.POST.get('name'), scenario="{}")
        return HttpResponseRedirect('/analitic/' + str(scenario.pk))
    else:
        return HttpResponse()


@group_required('analitics', raise_exception=True)
@csrf_exempt
def scenario_update(request, scenario_id):
    if request.method == 'POST':
        scenario_json = json.loads(request.body)
        ServiceSchema.objects.filter(pk=scenario_id).update(scenario=json.dumps(scenario_json))
        result = { 'success': True }
        return JsonResponse(result)
    else:
        return HttpResponse()


