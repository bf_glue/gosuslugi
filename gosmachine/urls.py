from django.urls import path, re_path
from . import views 

urlpatterns = [
    path('', views.scenario_list),
    path('create/', views.scenario_create),
    re_path('(\d+)/$', views.scenario),
    re_path('(\d+)/update/$', views.scenario_update),
]
