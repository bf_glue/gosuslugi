STATES = {}


class DataObject(object):
    pass


class ScenarioBlock(object):
    is_auto = False
    is_ready = False
    debug_message = None


class State(ScenarioBlock):
    next_block = None

    @property
    def name(self):
        return self.__class__.__name__

    def enter(self, input_dataobj, params=None):
        return input_dataobj

    def proceed(self, input_dataobj, new_data, params=None):
        if self.debug_message:
            print(self.debug_message)
        return ({**input_dataobj, **new_data}, self.next_block)

    def interrupt(self, input_dataobj):
        if self.debug_message:
            print("Interrupt! -> " + self.debug_message)
        return (input_dataobj, self.next_block)


class StopState(State):
    next_block = None


class Condition(ScenarioBlock):
    is_auto = True
    field_name = ""
    operator = ""
    target_value = ""
    next_if_true = State()
    next_if_false = State()

    def compare(self, dataobj):
        if dataobj.get(self.field_name) is None:
            return False
        checking_value = dataobj.get(self.field_name)
        operator = self.operator
        if (operator == 'eq'):
            return checking_value == self.target_value
        elif operator == 'neq':
            return checking_value != self.target_value
        elif operator == 'gt':
            return checking_value > self.target_value
        elif operator == 'lt':
            return checking_value < self.target_value
        elif operator == 'gte':
            return checking_value >= self.target_value
        elif operator == 'lte':
            return checking_value <= self.target_value
        else:
            return False

    def proceed(self, dataobj, new_data={}, params=None):
        if self.debug_message:
            print(self.debug_message)
        result = self.compare(dataobj)
        if result == True:
            return (dataobj, self.next_if_true)
        else:
            return (dataobj, self.next_if_false)

    @property
    def debug_message(self):
        return "Checking if " + self.field_name + " " + self.operator + " " + str(self.target_value)


class Scenario(object):
    blocks = {}

    def __init__(self, script):
        for block_key, block_description in script['blocks'].items():
            if block_description['type'] == 'Condition':
                block = Condition()
                block.operator = block_description['operator']
                block.field_name = block_description['field_name']
                block.target_value = block_description['target_value']
            elif block_description['type'] == 'State':
                clz = STATES[block_description['name']]
                block = clz()
                if block_description.get('params'):
                    for param_key, param_value in block_description['params'].items():
                        block.__setattr__(param_key, param_value)

                if block_description.get('meta'):
                    block.meta = block_description['meta']

            self.blocks[block_key] = block

        for block_key, block in self.blocks.items():
            block.key = block_key
            if isinstance(block, Condition):
                block.next_if_true = self.blocks[script['links'][block_key]["1"]]
                block.next_if_false = self.blocks[script['links'][block_key]["0"]]
                # print(block)
            elif isinstance(block, State):
                if script['links'].get(block_key) is not None:
                    block.next_block = self.blocks[script['links'][block_key]]
                    # print(block)
        
        self.first = self.blocks[script['first']]


class Order(object):
    uid = None
    scenario = None
    current_state = None
    current_data = {}

    def __init__(self, s):
        self.scenario = s
        self.current_state = s.first

    def step(self, new_data, interrupt=False):
        if self.current_state == None:
            return False

        if not interrupt:
            result = self.current_state.proceed(self.current_data, new_data, None)
        else:
            result = self.current_state.interrupt(self.current_data, None)

        self.current_data = result[0]
        self.current_state = result[1]
        return True

    def goto(self, state_id, data):
        self.current_data = data
        if state_id is None:
            self.current_state = StopState()
        else:
            self.current_state = self.scenario.blocks[state_id]
    
