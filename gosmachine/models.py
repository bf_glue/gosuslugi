from django.db import models
from django.contrib.auth.models import User
from .abstract import Order, Scenario
import json

class ServiceSchema(models.Model):
    name = models.CharField(max_length=255)
    scenario = models.TextField()

    @property
    def scenario_object(self):
        return json.loads(self.scenario)


class UserOrder(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    scenario = models.ForeignKey(ServiceSchema, on_delete=models.CASCADE)
    current_state = models.CharField(max_length=255, null=True)
    current_data = models.TextField()

    _order_machine = None

    @property
    def current_data_object(self):
        return json.loads(self.current_data)

    @property
    def order_machine(self):
        if self._order_machine is None:
            scenario = Scenario(self.scenario.scenario_object)
            self._order_machine = Order(scenario)
            self._order_machine.goto(self.current_state, self.current_data_object)
        return self._order_machine

    @property
    def current_state_object(self):
        return self.order_machine.current_state

    @property
    def current_state_meta(self):
        try:
            return self.order_machine.current_state.meta
        except:
            return {}

    def step(self, new_data=None):
        if new_data is None:
            new_data = {}
    
        self.order_machine.step(new_data)

        new_state = self.order_machine.current_state
        
        while new_state != None and (new_state.is_auto or new_state.is_ready):
            self.order_machine.step({})
            new_state = self.order_machine.current_state

        if self.order_machine.current_state is None:
            self.current_state = None
        else:
            self.current_state = self.order_machine.current_state.key
        self.current_data = json.dumps(self.order_machine.current_data)
        self.save()

        return self.current_data