from .abstract import State, STATES
import random


class UserFormState(State):
    verbose_name = 'Внесение данных пользователем'

    def proceed(self, input_dataobj, new_data, params):
        field_name = "client_form_data"
        if params:
            field_name = params.get('field_name', "client_form_data")
        return super().proceed(input_dataobj, { field_name: new_data }, params)


class ConfirmationState(State):
    verbose_name = 'Ожидание подтверждения оператором'


class ProcessingState(State):
    verbose_name = 'Обработка оператором'
    def proceed(self, input_dataobj, new_data, params):
        field_name = "result"
        if params:
            field_name = params.get('field_name', "result")
        return super().proceed(input_dataobj, { field_name: new_data }, params)


class GosAPIState(State):
    verbose_name = 'Взаимодействие с API'

    @property
    def is_ready(self):
        return True

class ExtAPIState(State):
    verbose_name = 'Взаимодействие с API'

    @property
    def is_ready(self):
        return True


class CalculationState(State):
    verbose_name = 'Вычисления'
    is_auto = True


class SignState(State):
    verbose_name = 'Подпись'
    is_auto = True

    def proceed(self, input_dataobj, new_data, params):
        return super().proceed(input_dataobj, { "signature": '...' }, params)


class ResultState(State):
    verbose_name = 'Результат оказания услуги'


STATES['UserFormState'] = UserFormState
STATES['ConfirmationState'] = ConfirmationState
STATES['ProcessingState'] = ProcessingState
STATES['GosAPIState'] = GosAPIState
STATES['ExtAPIState'] = ExtAPIState
STATES['CalculationState'] = CalculationState
STATES['SignState'] = SignState
STATES['ResultState'] = ResultState

