"""gosuslugi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import include
from django.contrib import admin
from django.conf import settings

from .views import (
    router, login, logout,
    users_page, mfc, ministry, operator, operator_order,
    users_service, users_order, users_order_create, users_order_submit, operator_order_submit
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', router),

    url(r'^user/$', users_page),
    url(r'^user/(\d+)/$', users_service),
    url(r'^user/(\d+)/new-order/$', users_order_create),
    url(r'^user/order/(\d+)/$', users_order),
    url(r'^user/order/(\d+)/submit/$', users_order_submit),


    url(r'^analitic/', include('gosmachine.urls')),
    url(r'^mfc/', mfc),
    url(r'^ministry/', ministry),
    
    url(r'^operator/$', operator),
    url(r'^operator/(\d+)/$', operator_order),
    url(r'^operator/(\d+)/submit/$', operator_order_submit),

    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout),

]
