# -*- coding: utf-8 -*-
import json

from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.shortcuts import render

from django.contrib.auth import logout as django_logout, authenticate, login as django_login
from django.contrib.auth.decorators import login_required

from .utils import group_required

from gosmachine.models import ServiceSchema, UserOrder


def router(request):
    # ...
    user = request.user
    if user.groups.filter(name='users').exists():
        return HttpResponseRedirect('/user/')
    if user.groups.filter(name='analitics').exists():
        return HttpResponseRedirect('/analitic/')
    if user.groups.filter(name='mfcs').exists():
        return HttpResponseRedirect('/mfc/')
    if user.groups.filter(name='ministries').exists():
        return HttpResponseRedirect('/ministry/')
    if user.groups.filter(name='operators').exists():
        return HttpResponseRedirect('/operator/')

    return HttpResponseRedirect('/login/')


def login(request):
    if request.method == 'POST':
        if request.POST.get('username'):
            username = request.POST['username']
            password = request.POST['password']
            username = username.replace(' ', '')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                django_login(request, user)
                next = request.GET.get('next', '/')
                response = HttpResponseRedirect(next)
                return response
            else:
                user_exists = User.objects.filter(username=username, is_active=False)
                if user_exists:
                    return HttpResponseRedirect('/login/?not_active=1&username=' + username)
                else:
                    return HttpResponseRedirect('/login/?invalid=1&username=' + username)
        else:
            return HttpResponse("No username provided", status=403)
    else:
        return render(request, 'registration/login.html')


@login_required
def logout(request):
    django_logout(request)
    return HttpResponseRedirect('/login/')


@group_required('users', raise_exception=True)
def users_page(request):
    context = { 
        'space': 'user', 
        'services': ServiceSchema.objects.all(),
        'orders': UserOrder.objects.filter(user=request.user)
    }
    return render(request, 'users/index.html', context)


@group_required('users', raise_exception=True)
def users_service(request, service_id):
    context = { 
        'space': 'user', 
        'service': ServiceSchema.objects.get(pk=service_id)
    }
    return render(request, 'users/users_service.html', context)


@group_required('users', raise_exception=True)
def users_order_create(request, service_id):
    service = ServiceSchema.objects.get(pk=service_id)
    new_order = UserOrder.objects.create(
        user=request.user,
        scenario=service,
        current_data="{}",
        current_state=service.scenario_object['first']
    )
    return HttpResponseRedirect('/user/order/' + str(new_order.pk))


@group_required('users', raise_exception=True)
def users_order(request, order_id):
    order = UserOrder.objects.get(pk=order_id)
    context = { 
        'space': 'user', 
        'order': order,
    }
    return render(request, 'users/users_order.html', context)


@group_required('users', raise_exception=True)
def users_order_submit(request, order_id):
    raw_dict = request.POST.dict()
    del raw_dict['csrfmiddlewaretoken']
    order = UserOrder.objects.get(pk=order_id)
    order.step(raw_dict)
    return HttpResponseRedirect('/user/order/' + str(order_id))


# @group_required('analitics', raise_exception=True)
# def analitic(request):
#     context = { 'space': 'analitic' }
#     return render(request, 'analitics/index.html', context)


@group_required('mfcs', raise_exception=True)
def mfc(request):
    context = { 'space': 'mfcs' }
    return render(request, 'mfcs/index.html', context)


@group_required('ministries', raise_exception=True)
def ministry(request):
    context = { 'space': 'ministries' }
    return render(request, 'ministries/index.html', context)


@group_required('operators', raise_exception=True)
def operator(request):
    context = { 
        'space': 'operators',
        'orders': UserOrder.objects.all() 
    }
    return render(request, 'operators/index.html', context)


@group_required('operators', raise_exception=True)
def operator_order(request, order_id):
    context = { 'space': 'operators' }
    order = UserOrder.objects.get(pk=order_id)
    context['order'] = order
    return render(request, 'operators/order.html', context)


@group_required('operators', raise_exception=True)
def operator_order_submit(request, order_id):
    raw_dict = request.POST.dict()
    del raw_dict['csrfmiddlewaretoken']
    order = UserOrder.objects.get(pk=order_id)
    order.step(raw_dict)
    return HttpResponseRedirect('/operator/' + str(order_id))



